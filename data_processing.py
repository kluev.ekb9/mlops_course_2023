import numpy as np  # библиотека Numpy для операций линейной алгебры и прочего
import matplotlib.pyplot as plt  # библиотека MatPlotLib для визуализации
import seaborn as sns;
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder

sns.set()  # библиотека Seaborn для визуализации данных из Pandas


def show_dataframe_info(dframe):
    print(dframe.head(), "\n" * 3)
    print(dframe.info(), "\n" * 3)
    print(dframe.isnull().sum(), "\n" * 3)

#
def show_t_col(dframe, type_columns):
    print(dframe.loc[:, type_columns].head(), "\n" * 3)
    for type_col in type_columns:
        print(dframe[type_col].unique())


# Используется для категориальных признаков
def show_unique_values(dframe, type_columns):
    for type_col in type_columns:
        print(dframe[type_col].unique())


def data_type(dframe):
    cat_columns = []  # создаем пустой список для имен колонок категориальных данных
    num_columns = []  # создаем пустой список для имен колонок числовых данных

    for column_name in dframe.columns:  # смотрим на все колонки в датафрейме
        if dframe[column_name].dtypes == object:  # проверяем тип данных для каждой колонки
            cat_columns += [column_name]  # если тип объект - то складываем в категориальные данные
        else:
            num_columns += [column_name]  # иначе - числовые

    # важно: если признак категориальный, но хранится в формате числовых данных, тогда код не сработает корректно

    return cat_columns, num_columns


def bar_chart_single(dframe, target):
    fig, ax = plt.subplots(figsize=(4, 3))
    sns.countplot(x=target, data=dframe, hue=target, palette=["green", "red"], ax=ax)
    plt.show()


def bar_chart_map(dframe, target, second_feature):
    g = sns.FacetGrid(dframe, col=target, sharey=False, height=4, aspect=1.2)
    g.map(plt.hist, second_feature,
          bins=range(dframe[second_feature].min(), dframe[second_feature].max() + 1,
                     1),
          alpha=0.7)
    g.set_axis_labels("Количество звонков в службу поддержки клиентов", "Колличество обращений")
    plt.subplots_adjust(top=0.82)
    plt.show()

def num_feature_graph(dframe, num_columns):
    fig = plt.figure(figsize=(16, 8))
    fig.suptitle('Графики числовых  признаков', fontsize=18, fontweight='bold')
    fig.subplots_adjust(top=0.92);
    fig.subplots_adjust(hspace=0.7, wspace=0.6);
    width = 5
    height = int(np.ceil(len(num_columns) / width))
    # fig = plt.subplots(nrows=height, ncols=width, figsize=(16,8)) # создаем "полотно", на котором будем "рисовать" графики
    # #     ↑  более точная структура (почти синоним subplot). Говорим что у нас будет height строк и width столбцов

    for idx, column_name in enumerate(num_columns):
        ax1 = plt.subplot(height, width, idx + 1)
        sns.histplot(data=dframe, x=column_name, ax=ax1);
        ax1.set_title(f'{column_name}')
        ax1.set_xlabel(f'{column_name}')
        ax1.set_ylabel('Count')
    plt.show()

def correlation(dframe):
    ax = round(dframe.corr()['churn'].sort_values(ascending=False)[1:], 2).plot(kind='bar', color='dodgerblue',
                                                                            figsize=(10, 8))
    ax.bar_label(ax.containers[0])
    plt.show()

def cat_to_num(dframe, dtest):
    # Заменим уникальные значения для построения графиков в area_code, voice_mail_plan, international_plan, churn
    dframe.area_code = dframe.area_code.map({'area_code_415': 415, 'area_code_408': 408, 'area_code_510': 510})
    dtest.area_code = dtest.area_code.map({'area_code_415': 415, 'area_code_408': 408, 'area_code_510': 510})

    dframe = dframe.replace({'voice_mail_plan': {'yes': 1, 'no': 0}})
    dtest = dtest.replace({'voice_mail_plan': {'yes': 1, 'no': 0}})

    dframe = dframe.replace({'international_plan': {'yes': 1, 'no': 0}})
    dtest = dtest.replace({'international_plan': {'yes': 1, 'no': 0}})

    dframe = dframe.replace({'churn': {'yes': 1, "no": 0}})
    dtest = dtest.replace({'churn': {'yes': 1, "no": 0}})

    return dframe, dtest

def matrix_correlation(dframe):
    fig = plt.figure(figsize=[7, 7])
    corr_matrix = dframe.corr()  # вычисление матрицы корреляции

    sns.heatmap(corr_matrix)
    plt.title('Матрица коререляции датафрейма')
    plt.show()

def data_rationing(dframe):
    scaler = StandardScaler()
    df_norm = scaler.fit_transform(dframe)
    df_norm = pd.DataFrame(df_norm, columns=dframe.columns)
    return df_norm


def vmail_cor(dfame):
    plt.figure(figsize=(8, 4))
    ax = sns.histplot(data=dfame, x='number_vmail_messages', bins=20);
    plt.title('Матрица коререляции')
    ax.set_xlabel('number_vmail_messages')
    ax.set_ylabel('Count')
    plt.show()
