l=range(10) #list
for i, ele in enumerate(l):print(i, ele)

print()

def foo(x):
    def bar(y):
        def baz(z):
            return x+y+z
        return baz(3)
    return bar(10)
print(foo(2))  # print(10+3+2)

print()


func = lambda x= 1, y= 2:lambda z: x + y + z #inner lambda function
f = func()
print(f(4))

print()

def calc(a, b):
    c = a + b
    d = c * 2
    print("Result is: " + str(d))
    return d

calc(3,4)