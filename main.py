import pandas as pd
from data_processing import *

# Данные для обучения и тестирования
df = pd.read_csv("C:/Users/kluev/Documents/mlops_3_sem/data/train.csv")
test = pd.read_csv("C:/Users/kluev/Documents/mlops_3_sem/data/test.csv")

show_dataframe_info(df)

cat_columns, num_columns = data_type(df)

print('Категориальные данные:\t ', cat_columns, '\n Число столблцов = ', len(cat_columns))
print('Числовые данные:\t ', num_columns, '\n Число столблцов = ', len(num_columns))

df, test = cat_to_num(df, test)

vmail_cor(df)

df = df.drop(columns=['state', 'number_vmail_messages', 'total_day_charge', 'total_eve_charge', 'total_night_charge',
                      'total_intl_charge'])

cat_columns, num_columns = data_type(df)

bar_chart_single(df, 'churn')

bar_chart_map(df, 'churn', 'number_customer_service_calls')

num_feature_graph(df, num_columns)

dfn = data_rationing(df)
